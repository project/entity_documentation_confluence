<?php

/**
 * @file
 * Main file for Entity Documentation Confluence module.
 */

/**
 * Implements hook_ed_exporter().
 */
function entity_documentation_confluence_ed_exporter($exporters) {

  // Add confluence exporter.
  $exporters['confluence'] = array(
    'name' => 'Confluence',
  );

  return $exporters;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function entity_documentation_confluence_form_entity_documentation_settings_form_alter(&$form, &$form_state, $form_id) {

  // Get entity types.
  $types = ed_get_entity_types();

  // The entity documentation settings form is visible only when at least node
  // entity type is available.
  if (count($types) != 0) {

    $form['auto_export']['ed_confluence'] = array(
      '#type' => 'fieldset',
      '#title' => t('Confluence'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => '5',
    );
    // Protocol select list.
    $form['auto_export']['ed_confluence']['protocol'] = array(
      '#type' => 'select',
      '#title' => t('Protocol'),
      '#options' => array(
        'http' => t('HTTP'),
        'https' => t('HTTPS'),
      ),
      '#default_value' => variable_get('ed_confluence_protocol', 'http'),
      '#required' => TRUE,
    );
    // Host name textfield.
    $form['auto_export']['ed_confluence']['host'] = array(
      '#type' => 'textfield',
      '#title' => t('Host'),
      '#default_value' => variable_get('ed_confluence_host', ''),
      '#required' => TRUE,
    );
    // Port number if there is one.
    $form['auto_export']['ed_confluence']['port'] = array(
      '#type' => 'textfield',
      '#title' => t('Port'),
      '#default_value' => variable_get('ed_confluence_port', ''),
    );
    // Basic auth username.
    $form['auto_export']['ed_confluence']['auth']['user_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => variable_get('ed_confluence_user_name', ''),
    );
    // Basic auth password.
    $form['auto_export']['ed_confluence']['auth']['password'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
    );
    // Confluence space key.
    $form['auto_export']['ed_confluence']['space_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Space Key'),
      '#description' => t('The confluence space key under witch the Entity Documentation pages will belong.'),
      '#default_value' => variable_get('ed_confluence_space_key', ''),
      '#required' => TRUE,
    );

    // Confluence parent id.
    $form['auto_export']['ed_confluence']['parent_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Parent page id'),
      '#description' => t('The confluence page id under witch the Entity Documentation pages will be added.'),
      '#default_value' => variable_get('ed_confluence_parent_id', ''),
      '#required' => TRUE,
    );

    $form['#validate'][] = 'entity_documentation_confluence_validate';
    $form['#submit'][] = 'entity_documentation_confluence_submit';

    return $form;
  }
}

/**
 * Form validation for input values.
 */
function entity_documentation_confluence_validate($form, &$form_state) {

  // Check if there is a port number.
  if ($form_state['values']['port'] != '') {
    $port = ':' . $form_state['values']['port'];
  }
  else {
    $port = '';
  }

  $user_name = $form_state['values']['user_name'];
  $pass = $form_state['values']['password'];
  $stored_pass = variable_get('ed_confluence_pass', '');
  $host = $form_state['values']['host'];

  // Check if user entered both username and password.
  if (($user_name != '') && ($pass == '' && $stored_pass == '')) {
    form_set_error('password', t('Password is required.'));
  }

  // Get url value.
  $url = $form_state['values']['protocol'] . '://' . $host . $port;

  // Check if url is valid.
  $absolute = valid_url($url, TRUE);
  $non_absolute = valid_url($url, FALSE);

  if ($absolute != TRUE && $non_absolute != TRUE) {
    form_set_error(array('protocol', 'host', 'port'), t('Invalid url.'));
  }

}

/**
 * Form submit handler for input values.
 */
function entity_documentation_confluence_submit($form, &$form_state) {

  // Get saved variables value.
  $vars = variable_get('ed_confluence_api_values');
  $saved_values = unserialize($vars);
  $variable = array();

  // Check if user entered a port number and add it to the variable array.
  if ($form_state['values']['port'] != '') {
    $port = ':' . $form_state['values']['port'];
    $variable['port'] = $form_state['values']['port'];
    variable_set('ed_confluence_port', $form_state['values']['port']);
  }
  else {
    $port = '';
  }

  // Check if user entered username and pass and add it to the variable array.
  if (($form_state['values']['user_name'] != '')) {
    $user_name = $form_state['values']['user_name'];
    $variable['user_name'] = $user_name;
    variable_set('ed_confluence_user_name', $user_name);

    // Save password.
    if ($form_state['values']['password'] != '') {
      $pass = $form_state['values']['password'];
      $hash_password = hash('md5', $pass, FALSE);
      $variable['password'] = $hash_password;
      variable_set('ed_confluence_pass', $hash_password);

      // Encode user name and password.
      $data = $user_name . ':' . $form_state['values']['password'];
      $encoded = base64_encode($data);
      $variable['encoded'] = $encoded;
      variable_set('ed_confluence_encoded', $encoded);
    }
    else {
      $variable['password'] = $saved_values['password'];
      $variable['encoded'] = $saved_values['encoded'];
    }
  }

  // Save parent id, Space key, host and protocol.
  variable_set('ed_confluence_parent_id', $form_state['values']['parent_id']);
  $variable['parent_id'] = $form_state['values']['parent_id'];

  variable_set('ed_confluence_space_key', $form_state['values']['space_key']);
  $variable['space_key'] = $form_state['values']['space_key'];

  variable_set('ed_confluence_host', $form_state['values']['host']);
  $variable['host'] = $form_state['values']['host'];

  variable_set('ed_confluence_protocol', $form_state['values']['protocol']);
  $variable['protocol'] = $form_state['values']['protocol'];

  // Construct url and save it to a variable.
  $url = $form_state['values']['protocol'] . '://' . $form_state['values']['host'] . $port;
  $variable['url'] = $url;
  variable_set('ed_confluence_url', $url);

  // Serialize variable and save it.
  $serialized = serialize($variable);
  variable_set('ed_confluence_api_values', $serialized);
}

/**
 * Implements hook_ed_documentation_export().
 */
function entity_documentation_confluence_ed_documentation_export($exporter, $entity, $bundle) {

  if ($exporter == 'confluence') {
    module_load_include('inc', 'entity_documentation_confluence', 'includes/entity_documentation_confluence.functions');

    // Check if user filled out the settings form.
    $variable = variable_get('ed_confluence_api_values');

    if ($variable != NULL) {
      $variable = unserialize($variable);

      // Confluence link triggers a documentation export with confluence
      // Restful API.
      $response = entity_documentation_confluence_export_documentation($entity, $bundle, $variable);

      switch ($response) {
        case '200':
          drupal_set_message(t('Confluence page Created successfully.'));
          break;

        default:
          drupal_set_message(t('Something went wrong please check error log.'), 'error');
          break;

      }
    }
    else {
      drupal_set_message(t('Please fill out the settings form.'), 'error');
    }

    drupal_goto('admin/config/development/entity-documentation', array(), '301');
  }
}

/**
 * Implements hook_ed_documentation_file_export().
 */
function entity_documentation_confluence_ed_documentation_file_export($exporter, $entity, $bundle, $file) {

  if ($exporter == 'confluence') {
    // Check if user filled out the settings form.
    $variable = variable_get('ed_confluence_api_values');
    if ($variable != NULL) {
      module_load_include('inc', 'entity_documentation_confluence', 'includes/entity_documentation_confluence.functions');
      $variable = unserialize($variable);
      entity_documentation_confluence_export_documentation($entity, $bundle, $variable);
    }
  }

}

/**
 * Implements hook_variable_info().
 */
function entity_documentation_confluence_variable_info($options) {
  $variables = array();

  // Entity Documentation Confluence API values.
  $variables['ed_confluence_api_values'] = array(
    'title' => t('Entity Documentation Confluence API values'),
    'default' => '0',
    'description' => t('Entity Documentation Confluence API values.', array(), $options),
  );

  $variables['ed_confluence_protocol'] = array(
    'title' => t('Entity Documentation Confluence Protocol'),
    'default' => 'http',
    'description' => t('Entity Documentation Confluence Protocol.', array(), $options),
  );

  $variables['ed_confluence_host'] = array(
    'title' => t('Entity Documentation Confluence host'),
    'default' => '',
    'description' => t('Entity Documentation Confluence host.', array(), $options),
  );

  $variables['ed_confluence_port'] = array(
    'title' => t('Entity Documentation Confluence Port'),
    'default' => '',
    'description' => t('Entity Documentation Confluence Port.', array(), $options),
  );

  $variables['ed_confluence_user_name'] = array(
    'title' => t('Entity Documentation Confluence User Name'),
    'default' => '',
    'description' => t('Entity Documentation Confluence User Name.', array(), $options),
  );

  $variables['ed_confluence_user_pass'] = array(
    'title' => t('Entity Documentation Confluence User Password'),
    'default' => '',
    'description' => t('Entity Documentation Confluence User password.', array(), $options),
  );

  $variables['ed_confluence_space_key'] = array(
    'title' => t('Entity Documentation Confluence Space Key'),
    'default' => '',
    'description' => t('Entity Documentation Confluence Space Key value.', array(), $options),
  );

  $variables['ed_confluence_parent_id'] = array(
    'title' => t('Entity Documentation Confluence parent id'),
    'default' => '',
    'description' => t('Entity Documentation Confluence parent page id.', array(), $options),
  );

  $variables['ed_confluence_encoded'] = array(
    'title' => t('Entity Documentation Confluence Encoded values'),
    'default' => '',
    'description' => t('Entity Documentation Confluence base 64 encode username and pass.', array(), $options),
  );

  $variables['ed_confluence_url'] = array(
    'title' => t('Entity Documentation Confluence Url'),
    'default' => '',
    'description' => t('Entity Documentation Confluence Url value.', array(), $options),
  );

  return $variables;
}

/**
 * Implements hook_theme().
 */
function entity_documentation_confluence_theme($existing, $type, $theme, $path) {

  switch ($type) {
    case 'module':
      $themes = array(
        'entity_documentation_confluence' => array(
          'template' => 'tpl/entity_documentation_confluence',
          'variables' => array(
            'description' => NULL,
            'properties' => NULL,
            'field_columns' => NULL,
            'fields' => NULL,
          ),
        ),
      );
      return $themes;
  }
}
